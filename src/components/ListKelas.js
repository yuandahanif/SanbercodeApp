import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import IIcon from 'react-native-vector-icons/Ionicons';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {color} from '../style';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default function ListKelas({
  data,
  size = 64,
  title = 'Kelas',
  indexStart = 0,
}) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>{title}</Text>
      </View>
      <View style={styles.content}>
        {data &&
          data.map((data, index) => {
            if (index >= indexStart && index < indexStart + 4) {
              return (
                <TouchableOpacity
                  onPress={data.onPress || null}
                  style={styles.icon}
                  key={data.name}>
                  {data.icon === 'ionicons' ? (
                    <IIcon name={data.iconName} size={size} color="white" />
                  ) : data.icon === 'none' ? (
                    <Image
                      source={data.iconName}
                      style={{width: size, height: size}}
                    />
                  ) : (
                    <MCIcon name={data.iconName} size={size} color="white" />
                  )}
                  <Text style={styles.kelasName}>{data.name}</Text>
                </TouchableOpacity>
              );
            }
          })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp('95%'),
    alignSelf: 'center',
    backgroundColor: color.blue,
    borderRadius: 12,
    overflow: 'hidden',
    marginBottom: 15,
  },
  header: {
    padding: 5,
    backgroundColor: color.darkblue,
  },
  title: {
    color: color.white,
    fontSize: hp('2.1%'),
  },
  content: {
    flexDirection: 'row',
    paddingVertical: 10,
    justifyContent: 'space-around',
    alignItems: 'flex-end',
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  kelasName: {
    justifyContent: 'center',
    fontSize: hp('2%'),
    color: color.white,
  },
});
