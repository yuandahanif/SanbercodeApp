import React from 'react';
// import Tugas1 from './screens/Tugas1';
// import Tugas2 from './screens/Tugas2';
// import Tugas3 from './screens/Tugas3';
// import Tugas4 from './screens/Tugas4';
import Tugas5 from './navigation';
import firebase from '@react-native-firebase/app';

export default function index() {
  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: 'AIzaSyChBp4OpG9Oeq0Yrnhck0FT2GVyB4fZ0JE',
    authDomain: 'sanbercode-8ab2e.firebaseapp.com',
    databaseURL: 'https://sanbercode-8ab2e.firebaseio.com',
    projectId: 'sanbercode-8ab2e',
    storageBucket: 'sanbercode-8ab2e.appspot.com',
    messagingSenderId: '738227167089',
    appId: '1:738227167089:web:9e667d4f1a8fd53c7ee0d0',
    measurementId: 'G-X45Q5L7MQN',
  };
  // Initialize Firebase
  // Inisialisasi firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }

  return <Tugas5 />;
}
