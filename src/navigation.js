import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Feather';
import auth from '@react-native-firebase/auth';

import {
  Login,
  OnBoarding,
  Register,
  SplashScreen,
  Home,
  Profile,
  Map,
  ReactNative,
  Chat,
} from './screens';
import {color as COLOR} from 'react-native-reanimated';

export default () => {
  const [showOnboard, setShowOnboard] = React.useState(true);
  const [splash, setSplash] = React.useState(true);
  const [isAuth, setIsAuth] = React.useState(false);

  const MainStack = createBottomTabNavigator();
  const Main = () => (
    <MainStack.Navigator
      initialRouteName="Home"
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;

          switch (route.name) {
            case 'Home':
              iconName = 'home';
              break;
            case 'Profile':
              iconName = 'user';
              break;
            case 'Map':
              iconName = 'map';
              break;
            case 'Chat':
              iconName = 'message-square';
              break;
            default:
              iconName = 'slash';
              break;
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: COLOR.darkblue,
        inactiveTintColor: 'gray',
      }}>
      <MainStack.Screen
        name="Home"
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="Map"
        component={Map}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="Chat"
        component={Chat}
        options={{headerShown: false}}
      />
      <MainStack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />
    </MainStack.Navigator>
  );

  const HomeStack = createStackNavigator();
  const HomeScreen = () => (
    <HomeStack.Navigator>
      <HomeStack.Screen name={'Home'} component={Home} />
      <HomeStack.Screen name={'ReactNative'} component={ReactNative} />
    </HomeStack.Navigator>
  );

  const AuthStack = createStackNavigator();
  const Auth = () => (
    <AuthStack.Navigator initialRouteName="Login" headerMode="none">
      <AuthStack.Screen name="Register" component={Register} />
      <AuthStack.Screen name="Login" component={Login} />
    </AuthStack.Navigator>
  );

  const AppStack = createStackNavigator();
  const App = () => (
    <AppStack.Navigator
      initialRouteName={showOnboard ? 'Intro' : isAuth ? 'Main' : 'Auth'}>
      <AppStack.Screen
        name="Intro"
        options={{headerShown: false}}
        component={OnBoarding}
      />
      <AppStack.Screen
        name="Auth"
        options={{headerShown: false}}
        component={Auth}
      />
      <AppStack.Screen
        name="Main"
        options={{headerShown: false}}
        component={Main}
      />
    </AppStack.Navigator>
  );

  const shuldShowOnboard = () => {
    AsyncStorage.getItem('skipOnboard')
      .then((value) => {
        value = value === null ? true : false;
        setShowOnboard(value);
        setSplash(false);
      })
      .catch((err) => {
        console.log('show onboard -> ', err);
      });
  };

  const checkIsAuth = () => {
    auth().onAuthStateChanged((user) => {
      if (user) {
        setIsAuth(true);
      }
    });
  };

  React.useEffect(() => {
    shuldShowOnboard();
    checkIsAuth();
  }, []);

  return (
    <NavigationContainer>
      {splash ? <SplashScreen /> : <App />}
    </NavigationContainer>
  );
};
