import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function index() {
  return (
    <View style={styles.container}>
      <Text>Halo Kelas React Native Lanjutan SanberCode!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
