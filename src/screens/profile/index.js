import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {vanue} from '../../api';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';

const dataDummy = {
  name: '',
  photo: null,
  birthDate: '21 Juni 2002',
  sex: 1,
  hobbies: ['Ngoding', 'Tidur'],
  phone: '+628139260813',
  email: 'yuan.nanode@gmail.com',
};

export default ({navigation}) => {
  const [data, setData] = useState(dataDummy);

  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        getVanue(token);
      } catch (error) {
        console.log('error -> getToken', error);
      }
    }
    // getToken();
    getCurrentUser();
  }, []);

  const getVanue = async (token) => {
    try {
      const res = await Axios.get(vanue, {
        timeout: 20000,
        headers: {
          Authorization: 'Bearer' + token,
        },
      });
      // setData(res.data.vanues);
    } catch (e) {
      console.log('error -> vanue', e);
    }
  };

  const getCurrentUser = async () => {
    try {
      const user = await GoogleSignin.signInSilently();
      // console.log('user -> ', user);
      setData((prevData) => {
        return {
          ...prevData,
          name: user && user.user && user.user.name,
          email: user && user.user && user.user.email,
          photo: user && user.user && user.user.photo,
        };
      });
    } catch (error) {
      console.log('get user error -> ', error);
    }
  };

  const logutPress = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      await auth().signOut();
      // delete local token
      await AsyncStorage.removeItem('token');
      navigation.reset({
        index: 0,
        routes: [{name: 'Auth'}],
      });
    } catch (error) {
      console.log('logout -> ', error);
      auth().signOut();
      navigation.reset({
        index: 0,
        routes: [{name: 'Auth'}],
      });
      // if (error.code === statusCodes.SIGN_IN_REQUIRED) {
      // }
    }
  };

  const {name, photo, birthDate, hobbies, phone, email, sex} = data;
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Image style={styles.profileImage} source={{uri: photo}} />
        <Text style={styles.profileName}>{name}</Text>
      </View>
      <View style={styles.content}>
        <View style={styles.bioData}>
          <TextBiodata lable="Tanggal Lahir" value={birthDate} />
          <TextBiodata
            lable="Jenis Kelamin"
            value={sex ? 'Laki-laki' : 'Perempuan'}
          />
          <TextBiodata lable="Hobi" value={hobbies.join(', ')} />
          <TextBiodata lable="No.Telp" value={phone} />
          <TextBiodata lable="Email" value={email} />
          <TouchableOpacity onPress={logutPress} style={styles.logoutButton}>
            <Text style={styles.logoutText}>Logout</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const TextBiodata = ({lable, value}) => {
  return (
    <View style={styles.textBioContainer}>
      <Text style={styles.textBio}>{lable}</Text>
      <Text style={styles.textBio}>{value}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    backgroundColor: '#3EC6FF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 100,
  },
  profileName: {
    marginTop: 10,
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white',
  },
  content: {
    flex: 2,
    alignItems: 'center',
  },
  bioData: {
    borderRadius: 6,
    marginTop: -20,
    backgroundColor: 'white',
    elevation: 3,
    width: '80%',
    padding: 10,
    paddingHorizontal: 20,
  },
  textBioContainer: {
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textBio: {
    fontSize: 16,
    fontWeight: '400',
  },
  logoutButton: {
    backgroundColor: '#3EC6FF',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoutText: {
    color: 'white',
    fontWeight: 'bold',
  },
});
