import Login from './auth/Login';
import Register from './auth/Register';
import SplashScreen from './splashscreen';
import OnBoarding from './obBoarding';
import Home from './home';
import Profile from './profile';
import Map from './map';
import ReactNative from './react native';
import Chat from './chat';

export {
  Login,
  SplashScreen,
  OnBoarding,
  Register,
  Home,
  Profile,
  Map,
  ReactNative,
  Chat,
};
