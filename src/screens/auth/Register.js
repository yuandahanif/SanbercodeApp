import React, {useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Modal,
  Pressable,
} from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
import {TouchableOpacity} from 'react-native-gesture-handler';
// import {GoogleSignin} from '@react-native-community/google-signin';
// import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [photo, setPhoto] = useState('https://i.pravatar.cc/300');
  const [cameraType, setCameraType] = useState('back');

  const camera = useRef(null);

  const takePhoto = async () => {
    const options = {quality: 0.5, base64: true};
    const data = await camera.current.takePictureAsync(options);
    // set photo to profile photo and hide the modal
    setPhoto(data.uri);
    setModalVisible(false);
  };

  const uploadPhoto = (uri) => {
    const id = new Date().getTime();
    const upload = storage().ref(`/images/${id}`).putFile(uri);

    upload.then(() => {
      alert('upload success!');
    });

    upload.on('state_changed', (progress) => {
      console.log(
        `transfer data ${progress.bytesTransferred} dari ${progress.totalBytes}`,
      );
    });

    upload.catch((err) => {
      console.log('error upload photo -> ', err);
    });
  };

  const toggleCameraType = () => {
    setCameraType((prevState) => (prevState === 'back' ? 'front' : 'back'));
  };

  const registerPress = async () => {
    uploadPhoto(photo);
    // try {
    //   navigation.reset({
    //     index: 0,
    //     routes: [{name: 'Login'}],
    //   });
    // } catch (error) {
    //   console.log('logout -> ', error);
    // }
  };

  return (
    <View style={styles.container}>
      <Modal
        // animationType="slide"
        style={{margin: 0}}
        // transparent={true}
        // presentationStyle="overFullScreen"
        // statusBarTranslucent={false}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}>
        <RNCamera style={styles.camera} ref={camera} type={cameraType}>
          <Pressable
            onPress={() => {
              toggleCameraType();
            }}>
            <Icon
              name="rotate-3d-variant"
              color="white"
              size={32}
              style={styles.rotateIcon}
            />
          </Pressable>
          <View style={styles.faceFrame} />
          <View style={styles.idCardFrame} />
          <Pressable onPress={() => takePhoto()}>
            <Icon
              name="camera"
              size={32}
              color="white"
              style={styles.cameraIcon}
            />
          </Pressable>
        </RNCamera>
      </Modal>
      <View style={styles.top}>
        <Image style={styles.profileImage} source={{uri: photo}} />
        <TouchableOpacity onPress={() => setModalVisible(true)}>
          <Text style={styles.changeImage}>Change Photo</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.content}>
        <View style={styles.bioData}>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Nama</Text>
            <TextInput
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
              value={email}
              onChangeText={(text) => setEmail(text)}
              style={styles.textInput}
              placeholder="Nama"
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Email</Text>
            <TextInput
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
              value={email}
              onChangeText={(text) => setEmail(text)}
              style={styles.textInput}
              placeholder="Email"
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Password</Text>
            <TextInput
              secureTextEntry={true}
              autoCompleteType="password"
              textContentType="password"
              value={password}
              onChangeText={(text) => setPassword(text)}
              style={styles.textInput}
              placeholder="Password"
            />
          </View>
          <TouchableOpacity onPress={registerPress} style={styles.logoutButton}>
            <Text style={styles.logoutText}>Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    backgroundColor: '#3EC6FF',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 100,
  },
  changeImage: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  camera: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 30,
    zIndex: 10,
  },
  rotateIcon: {
    alignSelf: 'flex-start',
    zIndex: 20,
  },
  faceFrame: {
    width: '35%',
    height: '25%',
    borderRadius: 60,
    borderColor: 'white',
    borderWidth: 2,
  },
  idCardFrame: {
    width: '65%',
    height: '20%',
    borderColor: 'white',
    borderWidth: 2,
  },
  // cameraIcon: {},
  content: {
    flex: 2,
    alignItems: 'center',
  },
  bioData: {
    borderRadius: 6,
    marginTop: -30,
    backgroundColor: 'white',
    elevation: 3,
    width: '80%',
    padding: 10,
    paddingHorizontal: 20,
  },
  inputContainer: {
    marginBottom: 15,
  },
  textInput: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingVertical: 5,
    marginTop: 5,
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#3EC6FF',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoutButton: {
    backgroundColor: '#3EC6FF',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoutText: {
    color: 'white',
    fontWeight: 'bold',
  },
});
