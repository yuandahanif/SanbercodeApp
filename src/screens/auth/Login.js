import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  Text,
  View,
} from 'react-native';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import auth from '@react-native-firebase/auth';
import TouchID from 'react-native-touch-id';

import {login} from '../../api';

export default function Login({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // ComponentDidMount / unMount
  useEffect(() => {
    googleSigninConfig();
  }, []);

  const saveToken = async (token) => {
    await AsyncStorage.setItem('token', token);
    navigation.push('Biodata');
  };

  const onLoginClick = () => {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        navigation.reset({index: 0, routes: [{name: 'Main'}]});
      })
      .catch((err) => {
        alert('Login gagal silahkan coba lagi');
      });
  };

  // const onLoginClick = async () => {
  //   try {
  //     const response = await Axios.post(login, {
  //       email,
  //       password,
  //     });
  //     saveToken(response.data.token);
  //   } catch (error) {
  //     console.log('error -> ', error);
  //     alert('Login gagal silahkan coba lagi');
  //   }
  // };

  const googleSigninConfig = () =>
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '738227167089-hejiq4o9uc7fqu57gft00f7vcu0el7ee.apps.googleusercontent.com',
    });

  const signInWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const {idToken} = await GoogleSignin.signIn();

      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);

      navigation.reset({index: 0, routes: [{name: 'Main'}]});
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('user membatalkan signIn');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('signIn sedang dalam proses');
        alert('sedang masuk, harap tunggu . . . ');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Tidak ada layanan google play service');
        alert('Tidak ada layanan google play service');
      } else {
        console.log('Jaringan/Sistem bermasalah', error);
      }
    }
  };

  const touchIdConfig = {
    title: 'Authentication required',
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel',
  };

  const signInWithFingerprint = () => {
    TouchID.authenticate('please login', touchIdConfig)
      .then(() => {
        alert('Authenticated Successfully');
        navigation.reset({index: 0, routes: [{name: 'Main'}]});
      })
      .catch((err) => {
        console.log(err);
        alert('Authentication Failed');
      });
  };

  const goToRegister = () => {
    navigation.push('Register');
  };

  return (
    <KeyboardAwareScrollView contentContainerStyle={{flex: 1}}>
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={require('../../assets/images/logo.jpg')}
        />
        <View style={styles.form}>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Username</Text>
            <TextInput
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
              value={email}
              onChangeText={(text) => setEmail(text)}
              style={styles.textInput}
              placeholder="Username or Email"
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Password</Text>
            <TextInput
              secureTextEntry={true}
              autoCompleteType="password"
              textContentType="password"
              value={password}
              onChangeText={(text) => setPassword(text)}
              style={styles.textInput}
              placeholder="Password"
            />
          </View>
          <TouchableOpacity style={styles.loginButton} onPress={onLoginClick}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>

          <View style={styles.lineContainer}>
            <View style={styles.line} />
            <Text>OR</Text>
            <View style={styles.line} />
          </View>

          <GoogleSigninButton
            style={{width: '100%', height: 48}}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={signInWithGoogle}
            disabled={null}
          />

          <TouchableOpacity
            style={[styles.loginButton, styles.fingerprintButton]}
            onPress={signInWithFingerprint}>
            <Text style={styles.loginText}>SignIn with Fingerprint</Text>
          </TouchableOpacity>

          <View style={styles.textRegisterContainer}>
            <Text>Belum Punya Akun? </Text>
            <TouchableOpacity
              style={styles.registerButton}
              onPress={goToRegister}>
              <Text style={styles.registerButtonText}>Buat akun</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  logo: {
    marginTop: 20,
    alignSelf: 'center',
  },
  form: {
    flex: 1,
    alignItems: 'stretch',
    paddingHorizontal: 25,
  },
  inputContainer: {
    marginBottom: 15,
  },
  textInput: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    paddingVertical: 5,
    marginTop: 5,
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#3EC6FF',
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginText: {
    color: 'white',
    fontWeight: 'bold',
  },
  fingerprintButton: {
    backgroundColor: '#191970',
  },
  lineContainer: {
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  line: {
    flex: 0.45,
    backgroundColor: 'gray',
    height: 1,
  },
  textRegisterContainer: {
    flexDirection: 'row',
    marginTop: 'auto',
    marginBottom: 15,
    alignSelf: 'center',
  },
  registerButton: {
    color: 'darkblue',
  },
  registerButtonText: {
    color: '#3EC6FF',
  },
});
