import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import {RootContex} from '../../contexts/index';

export default function TodoList() {
  const {textTodo, addTodo, todos, handleTextInput, deleteTodo} = useContext(
    RootContex,
  );
  const [localTodos, setLocalTodos] = useState([]);

  useEffect(() => {
    setLocalTodos(todos);
  }, [todos]);

  const EmptyTodo = () => (
    <View style={styles.emptyTodo}>
      <Text style={{color: '#6c6c6c'}}>
        Tidak ada todo, silahkan tambahkan todo.
      </Text>
    </View>
  );

  const FooterTodo = () => {
    <View style={styles.footerTodo}></View>;
  };

  return (
    <View style={styles.container}>
      <Text>Masukkan TodoList</Text>
      <View style={styles.header}>
        <TextInput
          placeholder="Masukkan Todo"
          defaultValue={textTodo}
          onChangeText={(text) => handleTextInput(text)}
          style={styles.textInput}
        />
        <TouchableOpacity onPress={addTodo} style={styles.addButton}>
          <Icon name="plus" size={24} />
        </TouchableOpacity>
      </View>
      <FlatList
        data={localTodos}
        keyExtractor={(data) => data.id.toString()}
        contentContainerStyle={styles.todoListContainer}
        renderItem={({item}) => (
          <View style={styles.todoContainer}>
            {console.log(todos)}
            <View>
              <Text>{item.date}</Text>
              <Text>{item.text}</Text>
            </View>
            <TouchableOpacity
              onPress={() => deleteTodo(item.id)}
              style={styles.deleteButton}>
              <Icon name="trash-2" size={24} />
            </TouchableOpacity>
          </View>
        )}
        ListEmptyComponent={EmptyTodo()}
        ListFooterComponent={FooterTodo()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '5%',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 15,
  },
  textInput: {
    width: '85%',
    borderColor: 'grey',
    borderWidth: 2,
  },
  addButton: {
    padding: 10,
    backgroundColor: 'cyan',
    width: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  todoListContainer: {
    // flex: 1,
    // backgroundColor: 'red',
  },
  todoContainer: {
    borderColor: '#C6C6C6',
    borderWidth: 5,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 5,
    borderRadius: 6,
  },
  emptyTodo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerTodo: {
    marginTop: 50,
  },
});
