/* eslint-disable react-hooks/rules-of-hooks */
import React, {useState} from 'react';
import {RootContex} from '../../contexts';
import TodoList from './TodoList';

export default function index() {
  const [textTodo, setTextTodo] = useState('');
  const [todos, setTodos] = useState([]);

  const handleTextInput = (text) => {
    setTextTodo(text);
  };

  const addTodo = () => {
    setTodos((prevState) => {
      setTextTodo('');
      const id =
        typeof prevState[prevState.length - 1] !== 'undefined'
          ? prevState[prevState.length - 1].id + 1
          : 0;
      const date = new Date().toLocaleDateString();
      return [...prevState, {id, text: textTodo, date}];
    });
  };

  const deleteTodo = (id) => {
    setTodos(() => {
      const newList = todos.filter((todo) => {
        return todo.id !== id;
      });
      return newList;
    });
  };
  return (
    <RootContex.Provider
      value={{
        textTodo,
        addTodo,
        todos,
        handleTextInput,
        deleteTodo,
      }}>
      <TodoList />
    </RootContex.Provider>
  );
}
