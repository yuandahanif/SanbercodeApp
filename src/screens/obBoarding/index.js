import React from 'react';
import {StyleSheet, Text, View, Image, StatusBar} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

export default function Intro({navigation}) {
  const slides = [
    {
      key: 1,
      title: 'Belajar Intensif',
      text:
        '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
      image: require('../../assets/images/working-time.png'),
    },
    {
      key: 2,
      title: 'Teknologi Populer',
      text: 'Menggunakan bahasa pemrograman populer',
      image: require('../../assets/images/research.png'),
    },
    {
      key: 3,
      title: 'From Zero to Hero',
      text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
      image: require('../../assets/images/venture.png'),
    },
    {
      key: 4,
      title: 'Training Gratis',
      text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
      image: require('../../assets/images/money-bag.png'),
    },
  ];

  //menampilkan data slides kedalam renderItem
  const renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} style={styles.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };

  //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
  const onDone = () => {
    AsyncStorage.setItem('skipOnboard', 'ok!').then(() =>
      navigation.navigate('Login'),
    );
  };

  //mengcustom tampilan button done
  const renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  //mengcustom tampilan next button
  const renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <View style={{flex: 1}}>
        <AppIntroSlider
          data={slides}
          onDone={onDone}
          renderItem={renderItem}
          renderDoneButton={renderDoneButton}
          renderNextButton={renderNextButton}
          keyExtractor={(item, index) => index.toString()}
          activeDotStyle={{backgroundColor: '#191970'}}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'cyan',
  },
  slide: {
    paddingHorizontal: 15,
    flex: 1,
    // backgroundColor: 'cyan',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'darkblue',
  },
  image: {
    // width: 80,
    marginVertical: 20,
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    color: 'grey',
  },
  buttonCircle: {
    padding: 10,
    backgroundColor: 'darkblue',
    borderRadius: 50,
  },
});
