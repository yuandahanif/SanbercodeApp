/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

export default function index() {
  const [list, setList] = useState([]);
  const [textTodo, setTextTodo] = useState('');

  const currentDate = () => new Date().toLocaleDateString();

  const addPress = () => {
    setTextTodo('');
    setList((pervList) => {
      const prevId =
        typeof pervList[pervList.length - 1] == 'undefined'
          ? 0
          : pervList[pervList.length - 1].id + 1;
      return pervList.concat({
        id: prevId,
        addDate: currentDate(),
        textTodo,
      });
    });
  };

  const deletePress = (id) => {
    setList((prevList) => {
      const newList = prevList.filter((todo) => {
        return todo.id !== id;
      });
      return newList;
    });
  };

  const EmptyTodo = () => (
    <View style={styles.emptyTodo}>
      <Text style={{color: '#6c6c6c'}}>
        Tidak ada todo, silahkan tambahkan todo.
      </Text>
    </View>
  );

  const FooterTodo = () => {
    <View style={styles.footerTodo}></View>;
  };

  return (
    <View style={styles.container}>
      <Text>Masukkan TodoList</Text>
      <View style={styles.header}>
        <TextInput
          placeholder="Masukkan Todo"
          defaultValue={textTodo}
          onChangeText={(text) => setTextTodo(text)}
          style={styles.textInput}
        />
        <TouchableOpacity onPress={addPress} style={styles.addButton}>
          <Icon name="plus" size={24} />
        </TouchableOpacity>
      </View>
      <FlatList
        data={list}
        keyExtractor={(data) => data.id.toString()}
        contentContainerStyle={styles.todoListContainer}
        renderItem={({item}) => (
          <View style={styles.todoContainer}>
            <View>
              <Text>{item.addDate}</Text>
              <Text>{item.textTodo}</Text>
            </View>
            <TouchableOpacity
              onPress={() => deletePress(item.id)}
              style={styles.deleteButton}>
              <Icon name="trash-2" size={24} />
            </TouchableOpacity>
          </View>
        )}
        ListEmptyComponent={EmptyTodo()}
        ListFooterComponent={FooterTodo()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '5%',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 15,
  },
  textInput: {
    width: '85%',
    borderColor: 'grey',
    borderWidth: 2,
  },
  addButton: {
    padding: 10,
    backgroundColor: 'cyan',
    width: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  todoListContainer: {
    // flex: 1,
    // backgroundColor: 'red',
  },
  todoContainer: {
    borderColor: '#C6C6C6',
    borderWidth: 5,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 5,
    borderRadius: 6,
  },
  emptyTodo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerTodo: {
    marginTop: 50,
  },
});
