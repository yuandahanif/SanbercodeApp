import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {FlatList} from 'react-native-gesture-handler';

import {color} from '../../style';
import Kelas from '../../components/ListKelas';

export default function index({navigation}) {
  const navigateTo = (screen) => {
    navigation.push(screen);
  };

  const dataKelas = [
    {
      name: 'React Native',
      icon: 'materialDesignIcons',
      iconName: 'react',
      totalStudents: 100,
      todayStudents: 20,
      onPress: () => navigateTo('ReactNative'),
    },
    {
      name: 'Data Science',
      icon: 'materialDesignIcons',
      iconName: 'language-python',
      totalStudents: 100,
      todayStudents: 30,
    },
    {
      name: 'React JS',
      icon: 'materialDesignIcons',
      iconName: 'react',
      totalStudents: 100,
      todayStudents: 40,
    },
    {
      name: 'Laravel',
      icon: 'ionicons',
      iconName: 'logo-laravel',
      totalStudents: 100,
      todayStudents: 50,
    },
    {
      name: 'Wordpress',
      icon: 'ionicons',
      iconName: 'logo-wordpress',
      totalStudents: 100,
      todayStudents: 20,
    },
    {
      name: 'Desain Grafis',
      icon: 'none',
      iconName: require('../../assets/images/website-design.png'),
      totalStudents: 100,
      todayStudents: 66,
    },
    {
      name: 'Web Server',
      icon: 'materialDesignIcons',
      iconName: 'server',
      totalStudents: 100,
      todayStudents: 45,
    },
    {
      name: 'UI/UX Design',
      icon: 'none',
      iconName: require('../../assets/images/ux.png'),
      totalStudents: 100,
      todayStudents: 50,
    },
  ];

  return (
    <FlatList
      data={dataKelas}
      contentContainerStyle={styles.flatList}
      ListHeaderComponent={() => (
        <View style={styles.header}>
          <Kelas data={dataKelas} />
          <Kelas data={dataKelas} indexStart={4} />
        </View>
      )}
      renderItem={({item, index}) => (
        <View
          style={[
            styles.infoGroup,
            index === 0 ? styles.roundedBorderTop : null,
            index === dataKelas.length - 1 ? styles.roundedBorderButtom : null,
          ]}>
          {index === 0 ? (
            <View
              style={[styles.headerGroup, {backgroundColor: color.darkblue}]}>
              <Text style={styles.titleGroup}>Summary</Text>
            </View>
          ) : null}
          <View style={styles.headerGroup}>
            <Text style={styles.titleGroup}>{item.name}</Text>
          </View>
          <View style={styles.contentGroup}>
            <Text style={styles.textGroup}>Today</Text>
            <Text style={styles.textGroup}>{item.todayStudents}</Text>
          </View>
          <View style={styles.contentGroup}>
            <Text style={styles.textGroup}>Total</Text>
            <Text style={styles.textGroup}>{item.todayStudents}</Text>
          </View>
        </View>
      )}
      keyExtractor={(data) => data.name}
    />
  );
}

const styles = StyleSheet.create({
  flatList: {
    minHeight: hp('100%'),
    padding: 10,
    backgroundColor: color.white,
  },
  roundedBorderTop: {
    borderTopRightRadius: 12,
    borderTopLeftRadius: 12,
  },
  roundedBorderButtom: {
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
  },
  infoGroup: {
    backgroundColor: color.darkblue,
    overflow: 'hidden',
  },
  headerGroup: {
    padding: 10,
    fontWeight: 'bold',
    backgroundColor: color.blue,
  },
  titleGroup: {
    color: color.white,
  },
  contentGroup: {
    alignSelf: 'center',
    flexDirection: 'row',
    width: wp('70%'),
    justifyContent: 'space-between',
  },
  textGroup: {
    paddingVertical: 5,
    color: color.white,
  },
});
