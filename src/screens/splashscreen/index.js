import React from 'react';
import {StyleSheet, View, Image} from 'react-native';

export default function Splashscreen() {
  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/images/logo.jpg')}
        style={styles.spalshLogo}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  spalshLogo: {
    // width: '70%',
    // height: 'auto',
  },
});
